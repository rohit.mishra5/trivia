import { QuestionType } from "../types/types"
export const constantText = {
  welcome:"Welcome To",
  trivia:"Trivia Quiz Games",
  question:"Question:",
  score:"Score:",
  correct_answer:"Coreect Answer",
  submit:"Submit",
  next:"Next",
  result:"Result"
}

export const emptyQuestion :QuestionType={
    category: '',
    correct_answer: '',
    difficulty: '',
    incorrect_answers: [],
    question: '',
    type: ''
  }
