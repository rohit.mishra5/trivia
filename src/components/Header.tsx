import React from 'react';
import {View,Text,StyleSheet, Dimensions} from 'react-native';
import { constantText } from '../constant/default';

export const Header = () => {
    return (
        <View style={styles?.container}>
            <Text style={styles?.headertext}>{constantText.welcome}</Text>
            <Text style={[styles?.headertext,{ fontSize: 35,}]}>{constantText.trivia}</Text>
        </View>
    )
}

const styles = StyleSheet.create<any>({
    container: {
        height:Dimensions.get('screen').height-700,
        backgroundColor:'#383732',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headertext: {
        fontSize: 30,
        width:350,
        fontWeight: '900', 
        color:'white'
    }
})

