import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  Image,
  Alert,
} from 'react-native';
import { emptyQuestion, constantText } from '../constant/default';
import { getQuestion } from '../services';
import { QuestionType } from '../types/types';

export const Questions = () => {
  const [page, setPage] = useState<number>(1);
  const [QuestionData, setQuestionData] = useState<QuestionType>(emptyQuestion);
  const [answer, setAnswer] = useState<string>('');
  const [answerData, setAnswerData] = useState<string>('');
  const [num, setNum] = useState<number>(1);
  const [marks, setMarks] = useState<number>(0);
  const [Error, setError] = useState<string>('');

  useEffect(() => {
    getQuestionApi();
  }, []);

  const getQuestionApi = () => {
    getQuestion(page, (data: any) => {
      setQuestionData(data.results[0]);
    });
  };
  const checkAnswer = (data: string) => {
    if (data) {
      setError('');
      data.toLowerCase().trim() === QuestionData?.correct_answer.toLowerCase()
        ? (setAnswerData('Correct'), setMarks(marks + 1))
        : setAnswerData('Incorrect');
    } else {
      setError('*Required field');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {QuestionData === emptyQuestion ? (
        <ActivityIndicator size="large" color="black" />
      ) : (
        <>
          <View
            style={styles.mainView}>
            <Text
              style={styles.currentQuestionText}>
              {constantText.question} {num}/10
            </Text>
            <Text
              style={styles.marksView}>
              {constantText.score} {marks}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', width: '100%', marginBottom: 10 }}>
            <Text style={styles.questionText}>Q{num}: </Text>
            <Text
              style={styles.questionDataText}>
              {QuestionData?.question}:
            </Text>
          </View>
          <View style={{ marginHorizontal: 0, marginTop: 0 }}>
            <TextInput
              style={[styles.inputAnswer]}
              value={answer}
              placeholder="Enter your Answer"
              onChangeText={setAnswer}
            />
            <Text style={{ color: 'red', fontSize: 15 }}>{Error}</Text>
          </View>
          {answerData !== '' && (
            <View style={{ width: '100%', paddingHorizontal: 15, marginTop: 20 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text
                  style={[
                    answerData !== 'Correct'
                      ? { color: 'red' }
                      : { color: 'green' },
                    { fontSize: 18, alignSelf: 'flex-start' },
                  ]}>
                  {answerData}
                </Text>
                <Image
                  source={
                    answerData !== 'Correct'
                      ? require('../assets/cross.png')
                      : require('../assets/checked.png')
                  }
                  style={{ width: 24, height: 24, marginLeft: 5 }}
                />
              </View>
              {answerData !== 'Correct' && (
                <Text
                  style={[
                    { color: 'green' },
                    { fontSize: 18, alignSelf: 'flex-start' },
                  ]}>
                  {constantText.correct_answer} :{QuestionData?.correct_answer}
                </Text>
              )}
            </View>
          )}

          {answerData === '' && (
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => {
                checkAnswer(answer);
              }}>
              <Text style={{ color: 'white', fontSize: 17 }}>{constantText.submit}</Text>
            </TouchableOpacity>
          )}

          {num < 10 && answerData !== '' && (
            <TouchableOpacity
              style={[
                styles.submitButton,
                {
                  borderColor: '#1a1302',
                  marginTop: 10,
                  backgroundColor: '',
                },
              ]}
              onPress={() => {
                setQuestionData(emptyQuestion);
                setNum(num + 1);
                setAnswerData('');
                setAnswer('');
                getQuestionApi();
              }}>
              <Text style={{ color: 'black' }}>{`${constantText.next} >>`}</Text>
            </TouchableOpacity>
          )}
          {num === 10 && answerData !== '' && (
            <TouchableOpacity
              style={[
                styles.submitButton,
                {
                  borderColor: 'black',
                  marginTop: 10,
                  backgroundColor: '#383732',
                },
              ]}
              onPress={() => {
                Alert.alert(`You Score - ${marks}`);
              }}>
              <Text style={{ color: 'white' }}>{constantText.result}</Text>
            </TouchableOpacity>
          )}
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create<any>({
  container: {
    flex: 1,
    height: Dimensions.get('window').height - 250,
    alignItems: 'center',
    paddingTop: 50,
    marginHorizontal: 20,
  },

  mainView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  currentQuestionText: {
    marginBottom: 20,
    fontSize: 15,
    textDecorationColor: 'black',
    textDecorationLine: 'underline',
  },
  marksView: {
    marginBottom: 20,
    fontSize: 15,
    textDecorationColor: 'black',
    textDecorationLine: 'underline',
  },
  questionDataText: {
    fontSize: 18,
    fontWeight: 'bold',
    width: '82%',
    marginLeft: 5,
    color: '#383732',
  },

  questionText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#383732',
  },
  inputAnswer: {
    borderWidth: 1,
    width: Dimensions.get('screen').width - 50,
    borderRadius: 15,
    paddingLeft: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
  submitButton: {
    backgroundColor: '#383732',
    alignItems: 'center',
    paddingHorizontal: 80,
    paddingVertical: 15,
    borderRadius: 15,
    marginTop: 40,
    width: Dimensions.get('screen').width - 50,
    borderWidth: 1,
  },
});
